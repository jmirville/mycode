terraform {
    required providers {
        docker = {
            source = "kreuzwerker/docker"
            version = "3.0.2"
            }
        }
}

provider "docker" {}

resource "docker_image" "simplegoservice" {
    name= "registry.gitlab.com/alta3/simplegoservice"
    keep_locally = true
}


resource "docker_container" "simplegoservice" {
    image = docker_image.simplegoservice.image_id
    name = 

